import React from 'react';
import './style.css';

const ContactMe = props => {
    return(
        <div style={{textAlign: "center", color: "white"/*, height: "100vh"*/}} className="pt-5 container">
            <p style={{fontSize: "1.5em"}}>Telefon: 504971449</p>
            <p  style={{fontSize: "1.5em"}}>Email: ewa.szewczak9999@gmail.com</p>
            <p  style={{fontSize: "1.5em"}}>Moje CV możesz obejrzeć <a href="Ewa_Szewczak.pdf" style={{color: "yellow"}}>tutaj</a></p>
        </div>
    );
}
export default ContactMe;
