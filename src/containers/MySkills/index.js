import React from 'react';
import './style.css';

const MySkills = props => {
    return(
        <div className="pt-5" style={{ color: "white"/*, height: "200vh"*/}}  >
           <h5 style={{textAlign: "center"}}>Technologie jakie używałam :</h5>
            <div className="container" >
                <div className="d-flex justify-content-center" >
                    <ul style={{listStyleType: "none"}}>
                        <li><p style={{fontSize: "15pt"}}><img src={"./symfony.png"} style={{height: "8vh"}}/>  Symfony</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./images.png"} style={{height: "8vh"}}/> Spring Boot</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./logo192.png"} style={{height: "8vh"}}/>  React</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./php.png"} style={{height: "8vh"}}/>  PHP</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./java2.png"} style={{height: "8vh"}} />Java</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./unnamed.png"} style={{height: "5vh"}} />  MySQL</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./html-5.png"} style={{height: "8vh", borderRadius: "50%"}}/>  HTML 5</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./cpp.png"} style={{height: "8vh"}} />C++</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./c.png"} style={{height: "8vh"}} />C</p></li>
                        <li><p style={{fontSize: "15pt"}}><img src={"./git.png"} style={{height: "8vh"}}/>Git</p></li>
                    </ul>
                </div>
            </div>
        </div>
    );
}
export default MySkills;
