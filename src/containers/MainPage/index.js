import React from 'react';
import './style.css';
import background from './scoby.png';
import background1 from './Kudlaty.png';
const MainPage = props => {

    return(
        <div /*id="main"*/ style={{ color: "white", /*height: "100vh",*/ textAlign: "center"}} >
            <div className="container">

                <section className="image" style={{height:"100%", width: "250px", backgroundImage: `url(${background})` }} ></section>
                <div className="container" style={{position: "absolute", top: "400px", left: "550px"}}>
		            <span className="react-logo">
                        <span className="nucleo"></span>
		            </span>
                </div>

                <section className="image" id="kudlaty" style={{height:"100%", width: "250px", backgroundImage: `url(${background1})` }} ></section>

                <p style={{fontSize: "1.5em", zIndex: "3", position: "relative"}} className="pt-5">Witam jestem Ewa. <i id="myDIV" className="far fa-smile-wink"></i></p>
                <p style={{fontSize: "1.5em",  zIndex: "3", position: "relative"}}>Aktualnie studiuję informatykę na I roku studiów magisterskich na Politechnice Lubelkiej. </p>
                <p style={{fontSize: "1.5em",  zIndex: "3", position: "relative"}}>Oto moja stronka.</p>
            </div>
        </div>
    );
}
export default MainPage;
