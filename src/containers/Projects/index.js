import React from 'react';
import './style.css';

const Projects = props => {
    return (
            <div className="container pt-2">
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    {/*<ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
                    </ol>*/}
                    <div className="carousel-inner" style={{width: "100%"}}>
                        <div className="carousel-item active">
                            <img className="d-block w-100" src="cukiernia2.png" alt="First slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia1.png" alt="Second slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia3.png" alt="Fourth slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia4.png" alt="Third slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia_kontakt1.png" alt="Fifth slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia_kontakt2.png" alt="Sixth slide"/>
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src="cukiernia_plan.png" alt="Eighth slide"/>
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                       data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button"
                       data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
        </div>
    );
}
export default Projects
