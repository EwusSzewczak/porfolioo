import logo from './logo.svg';
import './App.css';
import Navigation from './containers/Navigation/index';
import React from "react";
import MainPage from "./containers/MainPage";
import MySkills from "./containers/MySkills";
import ContactMe from "./containers/ContactMe";
import Projects from "./containers/Projects";
/*var myID = document.getElementById("main");
var myScrollFunc = function () {

    var y = window.scrollY;
    if (y >= 800) {
        myID.className += "show";
    } else {
        myID.className += " hide";
    }
};

window.addEventListener("scroll", myScrollFunc);*/

function App() {
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
    }
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        document.body.style.backgroundColor = "white";
    }

    let callScroll = function ScrollTo(id){
        window.alert(id);
        var scroll_to = document.getElementById(id);
        scroll_to.scrollIntoView({behavior: "smooth"});
    }

    function scroll(){
        document.getElementById("str").scrollTo(1000, 1000);
        closeNav();
    }


  return (
    <div className="App">
      <header className="App-header">
          {/*<Navigation/>*/}

          <div id="mySidenav" className="sidenav" style={{zIndex: "3"}}>
              <a href={{javascript:void(0)}} className="closebtn" onClick={closeNav}>&times;</a>
              <a href="#profile-section1" onClick={closeNav} >Strona główna</a>
              <a href="#profile-section2" onClick={closeNav} >O mnie</a>
              <a href="#profile-section3" onClick={closeNav} >Kotakt</a>
              <a href="#profile-section4" onClick={closeNav} >Projekty</a>
          </div>

          {/*<div id="mySidenav" className="sidenav">
              <a href={{javascript:void(0)}} className="closebtn" onClick={closeNav}>&times;</a>
              <a href={{javascript:void(0)}} onClick={callScroll(`profile-section1`)}>Strona główna</a>
              <a href={{javascript:void(0)}} onClick={callScroll(`profile-section2`)}>O mnie</a>
              <a href={{javascript:void(0)}} onClick={callScroll(`profile-section3`)}>Kotakt</a>
          </div>*/}
          <div id="main" style={{ display: "block"}} >
              <div style={{minHeight: "100vh"}}>
                  <div id="profile-section1" style={{ backgroundImage: "url(background.jpg)",  backgroundSize: "cover", borderBottom: "1px solid yellow", borderTop: "1px solid yellow" }}>
                    <span style={{fontSize: '30px', cursor: 'pointer', color: "black" }} onClick={(openNav)}>&#9776;</span>
                    <h1 style={{color: "gray", textAlign: "center"}}>Strona główna</h1>
                  </div>
                    <MainPage/>
              </div>
              <div style={{minHeight: "100vh",}}>
                  <div id="profile-section2"  style={{ backgroundImage: "url(background.jpg)",  backgroundSize: "cover", borderBottom: "1px solid yellow", borderTop: "1px solid yellow", zIndex: "3", position: "relative"  }} className="p-3">
                      <span style={{fontSize: '30px', cursor: 'pointer', color: "black"}} onClick={(openNav)}>&#9776;</span>
                      <h1 style={{color: "gray", textAlign: "center"}}>O mnie</h1>
                  </div>
                  <MySkills/>
              </div>


              <div style={{height: "100vh",}}>
                  <div id="profile-section3" style={{ backgroundImage: "url(background.jpg)",  backgroundSize: "cover", borderBottom: "1px solid yellow", borderTop: "1px solid yellow" }} className="p-3">
                      <span style={{fontSize: '30px', cursor: 'pointer', color: "black" }} onClick={(openNav)}>&#9776;</span>
                      <h1 style={{color: "gray", textAlign: "center"}}>Kontakt</h1>
                  </div>
                  <ContactMe/>
              </div>

          <div style={{height: "100vh",background: "#6b5d6b"}}>
              <div id="profile-section4" style={{ backgroundImage: "url(background.jpg)",  backgroundSize: "cover", borderBottom: "1px solid yellow", borderTop: "1px solid yellow" }} className="p-3">
                  <span style={{fontSize: '30px', cursor: 'pointer', color: "black" }} onClick={(openNav)}>&#9776;</span>
                  <h1 style={{color: "gray", textAlign: "center"}}>Projekty</h1>
              </div>
              <Projects/>
          </div>
          </div>

      </header>
    </div>
  );
}

export default App;
